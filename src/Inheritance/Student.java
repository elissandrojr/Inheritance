package Inheritance;

/**
 *
 * @author Elissandro.Jr
 */
public class Student extends Person {

    //Atributes
    private int SR;
    private double Note1;
    private double Note2;
    private int Frequency;

    //Empty Constructor
    public Student() {
        //System.out.println("Student Constructor");
    }

    public Student(String Name, int SR, double Note1, double Note2) {
        //From super class Person
        super.setName(Name);
        //From this class
        this.SR = SR;
        setNote1(Note1);
        this.Note2 = Note2;
    }

    //Getters
    public int getSR() {
        return SR;
    }

    public double getNote1() {
        return Note1;
    }

    public double getNote2() {
        return Note2;
    }

    public int getFrequency() {
        return Frequency;
    }

    //Setters
    public void setSR(int SR) {
        this.SR = SR;
    }

    public void setNote1(double Note1) {
        if (Note1 < 0) {
            System.out.println("Note can't be negative!");
        } else {
            this.Note1 = Note1;
        }

    }

    public void setNote2(double Note2) {
        if (Note2 < 0) {
            System.out.println("Note can't be negative!");
        } else {
            this.Note2 = Note2;
        }

    }

    public void setFrequency(int Frequency) {
        if (Frequency >= 0) {
            this.Frequency = Frequency;
        } else {
            System.out.println("Frequency must be >=0");
        }

    }

    public double grade() {
        double grade = (Note1 + Note2) / 2;
        return grade;
    }

    public boolean IsApproved() {
        if ((grade() >= 6.0) && (Frequency >= 75)) {
            return true;
        } else {
            return false;
        }

    }

    public String getInfoStudent() {
        String App;
        if (IsApproved()) {
            App = "Yes";
        }else{
            App = "No";
        }
        
        
        return "Student" + 1 + "\nName: " 
                + super.Name + "\nAge: " 
                + super.Age 
                + "\nWeight: " + super.Weight
                + "\nHeight: " + super.Height + "\nBMI: "
                + String.format("%.2f", super.BMI())
                + "\nGrade: "+grade()
                + "\nFrequency: " + this.Frequency 
                + "\nApproved: "+App;
    }
}
