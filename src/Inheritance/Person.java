package Inheritance;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author giovandemarco
 */
public class Person {

    //Class Attributes
    protected String Name;
    protected int Age;
    protected double Weight;
    protected double Height;

    //Constructor
    public Person() {
        //System.out.println("Person Constructor");
    }

    //Class Methods
    //Setters
    public void setName(String Name) {
        if (Name.isEmpty()) {
            System.out.println("Can't be empty!");
        } else {
            this.Name = Name;
        }

    }

    public void setAge(int Age) {
        if (Age >= 0) {
            this.Age = Age;
        } else {
            System.out.println("Age must be greater than zero!");
        }
    }

    public void setWeight(double Weight) {
        if (Weight >= 1.0) {
            this.Weight = Weight;
        } else {
            System.out.println("Weight must be greater or equal than 1 kilogram!");
        }
    }

    public void setHeight(double Height) {
        if (Height >= 0.15) {
            this.Height = Height;
        } else {
            System.out.println("Height must be greater o equal than 0.15 meters!");
        }
    }

    //Getters
    public String getName() {
        return Name;
    }

    public int getAge() {
        return Age;
    }

    public double getWeight() {
        return Weight;
    }

    public double getHeight() {
        return Height;
    }

    //getInfo - Method to retreive all class attributes
    public String getInfo() {
        return "Name:" + Name + "\nAge:" + Age + "\nWeight:" + Weight + "\nHeight:" + Height + "\nBMI:" + String.format("%.2f", BMI());
    }

    public double BMI() {
        if ((Weight >= 1.0) && (Height >= 0.15)) {
            //Valid values for calculate
            return Weight / (Height * Height);
        }
        //Can't calculate BMI
        return 0.0;
    }

}//end-class
