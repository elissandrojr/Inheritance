package Inheritance;

import java.util.Scanner;

/**
 *
 * @author Elissandro.Jr
 */
public class main {

    public static void main(String[] args) {
   
        Scanner read = new Scanner(System.in);
        int quant = 0;
        
        System.out.println("How many students do you want to register?");
        quant = read.nextInt();
        //Array to store Objects
        Student[] arrObj = new Student[quant];
        
        System.out.println("Register");
        //Inserting Objects Information
        for (int i = 0; i < arrObj.length; i++) {
            //Creanting an Object
            Student newObj = new Student();
            System.out.print("Name:");
            newObj.setName(read.next());
            System.out.print("Age:");
            newObj.setAge(read.nextInt());
            System.out.print("Height:");
            newObj.setHeight(read.nextDouble());
            System.out.print("Weight:");
            newObj.setWeight(read.nextDouble());
            System.out.println("Student Register (SR):");
            newObj.setSR(read.nextInt());
            System.out.println("Note1");
            newObj.setNote1(read.nextDouble());
            System.out.println("Note2");
            newObj.setNote2(read.nextDouble());
            System.out.println("Frequency");
            newObj.setFrequency(read.nextInt());
            //Adding the object into Array
            arrObj[i] = newObj;
        }
        
        System.out.println("INFORMATIONS");
        //Printing Array Objects Information from Student Class
        for (int i = 0; i < arrObj.length; i++) {
            System.out.println("Object" + "\n" + arrObj[i].getInfoStudent());
        }
    }

}
